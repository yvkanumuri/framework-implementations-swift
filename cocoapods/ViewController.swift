//
//  ViewController.swift
//  cocoapods
//
//  Created by yeswanth varma on 3/24/17.
//  Copyright © 2017 yeswanth varma. All rights reserved.
//

import UIKit
import Lottie
import IntentKit

class ViewController: UIViewController {
let animationView = LOTAnimationView(name: "permission")
    let v = "2.0"
    let mailHandler = INKMailHandler()
    let mapsHandler = INKMapsHandler()
    let urlHandler = INKBrowserHandler()
    let defaultsController = INKDefaultsViewController()
    var presenter = INKActivityPresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defaultsController.allowedHandlers = [mailHandler]
        
        //self.view.addSubview(animationView!)
//        wordLayout()
//        animationView?.translatesAutoresizingMaskIntoConstraints = false
//        view.backgroundColor = .black
//        animationView?.play(completion: { finished in
//            // Do Something
//        })
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func sendEmail(_ sender: Any) {
        let feedbackSubject = "DSNY iOS App Feedback "
        
        let body = "Version \(v)"
        let feedurl = "webdev@dsny.nyc.gov"
        mailHandler.messageBody = "test"
        mailHandler.subject = "test ios"
        presenter =  mailHandler.sendMail(to: feedurl)
        presenter.presentModalActivitySheet(from: self) {
            print("test")
        }
    }
    
    
    @IBAction func openMaps(_ sender: Any) {
        
        let dsnyurl:URL =  URL(string: "http://www1.nyc.gov")!
        presenter = urlHandler.open(dsnyurl)//mapsHandler.search(forLocation: "33 whitehall st")
        presenter.presentModalActivitySheet(from: self) {
            print("test")
        }
        
    }
    
    
    func wordLayout(){
        animationView?.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        animationView?.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        animationView?.rightAnchor.constraint(equalTo: self.view
            .rightAnchor).isActive = true
        animationView?.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

